module.exports = {
  version: "0.0.1",
  Profile: {
    externalID: "0112241",
    name: "TDPK",
    nameTH: "ทีดีพีเค",
    phoneNo: ["0928818482", "0948100224"],
    businessType: "Openlab", // dropdown businessType
    isInternal: true,
    applicationDate: "DATE/TIME",
    employeeNo: 100,
    accountLogo: "https://somedomain.com/logo.png",
    websiteURL: "https://somedomain.com/",
    preference: "N / A",
    parentID: "UUID"
  },
  Lease: {
    agreementNo: "N / A",
    leaseExpiryDate: "DATE/TIME",
    leaseCommencementDate: "DATE/TIME",
    rentCommencementDate: "DATE/TIME",
    lettingRatePerSqm: 1200, //bahtPerMonth?
    space: 50, //sqm
    totalRent: 12000000 //total per month (lettingRatePerSqm*space)
  },
  Facility: {
    firstOffsite: "DATE/TIME",
    finalOffsite: "DATE/TIME",
    managementServiceFee: 0, // bahtPerMonth default 0 baht
    handoverCondition: "Bare-shell", // dropdown handoverCondition nullable
    moverDate: "DATE/TIME",
    moveInDate: "DATE/TIME",
    officeOptHours: {
      start: "TIME",
      end: "TIME"
    },
    airconOptHours: {
      start: "TIME",
      end: "TIME"
    },
    regularOTAircon: "N / A",
    cleaningServiceSchedule: "N / A",
    consentLetterIssued: "N / A",
    maxDeskChair: "N / A",
    amountIssuedAccessCard: 2000, //cards
    hasAccessControl: false,
    hasServerRoom: true
  },
  Credit: {
    auditorium: 120, //hours
    meetingRoom: 2000, //PerMonth?
    maxAccessCard: 300, //cards
    carParkingQuota: "N / A", // by hours
    carParking: "N / A",
    workshop: 80 //hours
  },
  PIC: {
    TDPK: [
      {
        name: "P' Host", //name of person
        type: "hospitality" //type
      },
      {
        name: "P' Comm",
        type: "commercial"
      }
    ],
    account: [
      // type: ['finance', 'keyAuthorize', '24/7', 'admin/hr', 'crowdControl']
      {
        name: "FullName",
        position: "TheirPosition",
        email: "email@domain.com",
        type: "fiance"
      },
      {
        name: "FullName",
        position: "TheirPosition",
        email: "email@domain.com",
        type: "keyAuthorization"
      }
    ]
  },
  Addresses: {
    TDPK: {
      axis: "X", //dropdown axis
      level: "9", //dropdown floor
      unit: "N / A"
    },
    Billing: {
      unitNo: "",
      floor: "",
      building: "",
      street: "",
      subDistrict: "",
      district: "",
      province: "",
      country: "",
      postcode: ""
    },
    Shipping: {
      unitNo: "",
      floor: "",
      building: "",
      street: "",
      subDistrict: "",
      district: "",
      province: "",
      country: "",
      postcode: ""
    }
  },
  System: {
    lastActivity: "",
    lastModifiedByID: "",
    lastModifiedDate: "DATE/TIME",
    createdBy: "fullName",
    createdDate: "DATE/TIME"
  }
};
