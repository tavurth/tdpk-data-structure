const Joi = require("@hapi/joi");

const data = require("./sample");
const schema = require("./schema");

function tryValidate(data) {
  const result = schema.validate(data);
  if (result.error) {
    return console.error(
      result.error.details.map(({ message, path }) => ({ message, path }))
    );
  }

  console.log(result);
}

tryValidate(data);
