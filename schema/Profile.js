const Joi = require("@hapi/joi");
const { businessType } = require("./Selectors");

module.exports = Joi.object().keys({
  externalID: Joi.string(),
  name: Joi.string(),
  nameTH: Joi.string(), // TDPK
  phoneNo: Joi.array().items(Joi.number()), // +66 3319 128391
  employeeNo: Joi.number(), // 100
  isInternal: Joi.bool(),
  businessType
  /* businessType: "Openlab", // dropdown businessType
   * applicationDate: "DATE/TIME",
   * accountLogo: "https://somedomain.com/logo.png",
   * websiteURL: "https://somedomain.com/",
   * preference: "N / A",
   * parentID: "UUID" */
});
