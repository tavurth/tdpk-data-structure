const Joi = require("@hapi/joi");

const Profile = require("./Profile");

module.exports = Joi.object().keys({
  version: Joi.string(),
  Profile
});
