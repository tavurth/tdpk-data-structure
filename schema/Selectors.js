const Joi = require("@hapi/joi");

//dropdown data
//for business type
const businessType = Joi.string().valid(
  "Corporate",
  "Government Bodies",
  "Startups",
  "Services",
  "Investors",
  "Academics",
  "Openlab",
  "Other"
);

//for handover condition
const handoverCondition = Joi.string().valid(
  "Bare-shell",
  "Campus Office",
  "Other"
);

//PIC
const TPDKPICType = Joi.string().valid("hospitality", "commercial");
const TPDKHospitalityPIC = Joi.string().valid(
  "Pui",
  "Minkchi",
  "Mink-Mut",
  "IM",
  "Jus",
  "Other"
);

const accountPICType = Joi.string().valid(
  "finance",
  "keyAuthorize",
  "24/7",
  "admin/hr",
  "crowdControl"
);

//TDPK Address
const axis = Joi.string().valid("X", "Y", "Z", "Other");

const floor = Joi.string().valid(
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15"
);

module.exports = {
  axis,
  floor,
  TPDKPICType,
  businessType,
  accountPICType,
  handoverCondition,
  TPDKHospitalityPIC
};
